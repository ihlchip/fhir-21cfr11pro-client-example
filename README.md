# 21CFR11-Compliant FHIR Client Tester

## Introduction

This is a client that simulates a couple of scenarios for storing signed PRO resources with accompanying digital 
signatures as Provenance resources such that 21 CFR 11 requirements for provenance, auditability and non-repudiation
can be met. The client is a command line utility that's a useful integration test and an illustration of the 
21 CFR 11-compliant PRO FHIR implementation guide.

## Links

- 21CFR11 addendum to the FHIR PRO Implementation Guide - https://github.com/chb/21cfr11pro-ig
  - Published Site:  https://chb.github.io/21cfr11pro-ig/
- The KeyCloak proxy authentication server: https://bitbucket.org/ihlchip/keycloak-auth-bch
- The example client: https://bitbucket.org/ihlchip/fhir-21cfr11pro-client-example 
- The example 21CFR11-compliant FHIR Server that this client communicated with: https://bitbucket.org/ihlchip/hapi-fhir-jpaserver-oauth-21cfr11

## Components

The client communicates with an authentication server that is an authentication and credential-checking
gateway to the FHIR server. The keycloak-auth-bch project is a Spring Boot Java web application that checks and marshalls 
authentication requests using a background KeyCloak instance, and certificate signing requests through to a library of 
functions that validate that the pricipal that authenticated with OAuth is the same as the principal asking for 
the signing service to sign a client certificate.

Finally the HAPI FHIR server is an OAuth-protected resource server in OAuth and OIDC terms. Any client requires an 
IdP-issued and signed token to access the API.  

![System Context](doc/images/oauth-auth-context.png)

Not shown are the ImmuGW and ImmuDB servers that capture an immutable tamper-evident journal of FHIR resource 
changes. See the [Example 21CFR11 FHIR PRO Server](https://bitbucket.org/ihlchip/hapi-fhir-jpaserver-oauth-21cfr11) 
for more detail.

## Sequence of API Calls   

The test client uses a pre-provisioned user and password to login to the AuthServer authentication service. A more 
sophisticated system might provide self-service enrolment for study subjects, but in our client an administrator has 
provisioned the user and their credentials into the IdProvider in advance.

When the client application has logged in, it can generate an identity certificate or use one that is prepared earlier. 
When the certificate is available it creates a standard certificate request and sends it to the AuthServer's 
signing (`/sign`) endpiont. 

The AuthServer's signing endpoint examines the signing request to determine (at a minimum) that the principal in the certificate 
to be signed is the same as the principal that has already authenticated with the AuthServer. More sophisticated 
implementations may have other checks, or use a proxy for real pricipal names to verify the identity of the 
requestor.

More sophisticated signing services might operate a Managed PKI (MPKI) certifying authority backed by a 
security services vendor instead of a self-signed CA.

If the AuthServer is satisfied with the request then it signs the certificate using its private key and returns the 
identity certificate in the response. 

Now the client has a certificate signed by a CA that it can provide to systems that need to verify the provenance of 
data, like auditors of a FHIR server storing PRO data.

The client `POST`S a copy of its newly-signed certificate to the FHIR server as an embedded value in a `DocumentReference`
resource. It then uses the returned resource JSON to broadly verify the accurate representation of the `DocumentReference`
resource and generates a `Provenance` resource containing a digital signature for a canonical version of the 
`DocumentReference` resource.

Once the certificate is stored on the FHIR server and the Provenance for that certificate is stored, the client can 
POST PRO QuestionnareResponse resources in the same way. First sending the Questionnaire Response, examining the 
returned JSON with the FHIR server's fixed-up version of the resource, then generating a digital signature of the 
canonical JSON version of the resource to include in a POST-ed Provenance resource. 

An auditor can use the client's ID certificate to validate any resource against its corresponding Provenance digital 
signature.  

![Client flow diagram](doc/images/client-flow.png)

## License

Copyright 2020 Boston Children's Hospital Informatics Program

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions 
of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
DEALINGS IN THE SOFTWARE.