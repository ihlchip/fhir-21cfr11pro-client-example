/*
 * Copyright (c) 2020. Boston Children's Hospital
 */

package org.chip.ihl.proclient;

import ca.uhn.fhir.parser.IParser;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.api.PreferReturnEnum;
import lombok.extern.java.Log;
import org.apache.commons.cli.*;


import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import org.bouncycastle.asn1.pkcs.CertificationRequest;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.OperatorCreationException;
import org.chip.ihl.services.FHIRAtWill;
import org.chip.ihl.services.QuestionnaireResponseService;
import org.chip.ihl.services.ProvenanceService;
import org.chip.ihl.utility.OAuthClientUtil;
import org.chip.ihl.utility.StatusOutput;
import org.hl7.fhir.dstu3.model.DocumentReference;
import org.hl7.fhir.dstu3.model.Patient;
import org.hl7.fhir.dstu3.model.QuestionnaireResponse;
import org.hl7.fhir.instance.model.api.IIdType;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.Security;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Base64;
import java.util.logging.Level;

/**
 * Lombok logging shortcut
 */
@Log
public class ProClient {

    /**
     *
     * NB: Many of these constants would be configurable in a production client.
     *
     */
    static final String TOKEN_OPTION = "token";
    static final String URL_OPTION = "url";
    static final String USERID_OPTION = "userid";
    static final String PASSWORD_OPTION = "password";
    static final String AUTH_URL_OPTION = "authurl";
    static final String SIGNING_URL_OPTION = "signingurl";
    static final String DEFAULT_UNAUTHENTICATED_URL =  "http://hapi.fhir.org/baseDstu3";
    static final String APP_NAME =  "PROClient";
    static final String PRIVATE_KEY_FILE_NAME = "test-certs/key.pem";
    static final String CERTIFICATE_FILE_NAME = "test-certs/cert.pem";

    /**
     * Convenience function for extracting the option that comes with a command line argument.
     *
     * @param cmd the commandline to parse
     * @param optionKey The key to parse for
     * @param defaultValue What to set the option to if it is not found in the command line
     * @return
     */
    public static String getOptValue(CommandLine cmd, String optionKey, String defaultValue) {
        String optionValue = cmd.hasOption(optionKey) ? cmd.getOptionValue(optionKey) : defaultValue ;
        return optionValue;
    }

    /**
     *
     * This is a big main() to try to make the example transaction flow reasonably evident and without
     * too much sleight of hand. The scenario shown is necessarily a simplification. The basics are:
     *
     * 1. Use the username and password to obtain a bearer token for communicating with resource servers like the
     * OAuth2/Keycloak-protected HAPI FHIR server.
     *
     * 2. Use the auth server to sign an identity certificate.
     *
     * 3. a. Send the identity certificate to the HAPI FHIR server as a DocumentReference resource
     *    b. Verify the resource persisted by the HAPI FHIR server is canonically the same resource as was sent
     *    c. Generate an external signature for the resource returned and encapsulate it in a Provenance resource
     *    d. Send the Provenance resource to the server
     *
     * 4. a. Generate a QuestionnaireResponse and send it to the HAPI FHIR Server
     *    b. Verify the resource persisted by the HAPI FHIR server is canonically the same resource as was sent
     *    c. Generate an external signature for the resource returned and encapsulate it in a Provenance resource
     *    d. Send the Provenance resource to the server
     *
     * @param args Command line arguments
     */

    public static void main(String[] args) {

        Security.addProvider(new BouncyCastleProvider());

        OAuthClientUtil oauthClientUtil = new OAuthClientUtil();
        Options options = new Options();

        Option tokenOpt = new Option("t", TOKEN_OPTION, true, "bearer token");
        tokenOpt.setRequired(false);
        options.addOption(tokenOpt);

        Option urlOpt = new Option("u", URL_OPTION, true, "FHIR server URL (defaults to http://hapi.fhir.org/baseDstu3)");
        urlOpt.setRequired(false);
        options.addOption(urlOpt);

        Option nameOpt = new Option("i", USERID_OPTION, true, "Userid for authentication (token will not be used if userid is provided)");
        nameOpt.setRequired(false);
        options.addOption(nameOpt);

        Option passwordOpt = new Option("p", PASSWORD_OPTION, true, "Password for authentication");
        passwordOpt.setRequired(false);
        options.addOption(passwordOpt);

        Option authUrlOpt = new Option("a", AUTH_URL_OPTION, true, "URL for username/password authentication");
        authUrlOpt.setRequired(false);
        options.addOption(authUrlOpt);

        Option signingUrlOpt = new Option("s", SIGNING_URL_OPTION, true, "URL for certificate signing (e.g. http://localhost:8080/sign)");
        signingUrlOpt.setRequired(false);
        options.addOption(signingUrlOpt);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd = null;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp(APP_NAME, options);

            System.exit(1);
        }

        String url = getOptValue(cmd, URL_OPTION, DEFAULT_UNAUTHENTICATED_URL);
        String userId = getOptValue(cmd, USERID_OPTION, "");
        String password = getOptValue(cmd, PASSWORD_OPTION, "");
        String token = getOptValue(cmd, TOKEN_OPTION, "");
        String authUrl = getOptValue(cmd, AUTH_URL_OPTION, "");
        String signingUrl = getOptValue(cmd, SIGNING_URL_OPTION, "");


        log.info(String.format("Obtaining token with username=%s, password (containsdata=%B), url=%s", userId, !password.isEmpty(), authUrl));

        if (!(userId.isEmpty() || password.isEmpty() || authUrl.isEmpty() || url.isEmpty())) {
            token = oauthClientUtil.getToken(userId,password,authUrl);
        }

        // Create a context
        FhirContext ctx = FhirContext.forDstu3();
        IParser jsonParser = ctx.newJsonParser().setPrettyPrint(true);

        // Create a client
        IGenericClient client = ctx.newRestfulGenericClient(url);
        if (!token.isEmpty()) {
            log.log(Level.FINE, token);
            oauthClientUtil.registerauthInterceptor(client, token);
        }


        // ***
        // = 1. Generate a Patient resource for this client =
        //***
        StatusOutput.progressConsole(1, 1, "Generate a Patient resource for this client.\nThis is the resource we send to the FHIR server:\n");

        // Create a patient with the given ID
        Patient patient = new Patient();
        patient.addIdentifier()
                .setSystem("CN") // NC = invented X.509 Canonical Name system. This could be PT instead (patient external identifier)
                .setValue(userId); // Our patient's principal identifier which is the CN= in our certificate
        patient.addName().setFamily("Patel").addGiven("Sangeetha").addGiven("Geetha");
        StatusOutput.progressConsole(1, 2, jsonParser.encodeResourceToString(patient));


        // ***
        // = 2. Push the Patient resource to the server =
        //***

        StatusOutput.progressConsole(2, 1, "Push the Patient resource to the server");

        MethodOutcome outcome = client.create()
                .resource(patient)
                .prettyPrint()
                .encodedJson()
                .prefer(PreferReturnEnum.REPRESENTATION)
                .execute();

        // Print the output
        IIdType id = outcome.getId();
        Patient createdPatientResource = (Patient) outcome.getResource();

        String patientResourceAsString = jsonParser.encodeResourceToString(createdPatientResource);
        log.fine("This is the \"me\" patient resource:\n" + patientResourceAsString);

        StatusOutput.progressConsole(2, 2, "This is the server's 'fixed-up' version of the resource from the response");

        StatusOutput.progressConsole(2, 3, patientResourceAsString);

        // ***
        // = 3. Get our client certificate signed so we can use the signed cert as an identity certificate =
        //***

        StatusOutput.progressConsole(3, 1, "Get our client certificate signed so we can use the signed cert as an identity certificate");

        // Grab our certificate and ask for it to be signed

        try {
            X509Certificate cert = oauthClientUtil.pemToCert(CERTIFICATE_FILE_NAME);
            PrivateKey pk = oauthClientUtil.getPrivateKey(PRIVATE_KEY_FILE_NAME);
            // TODO: Refactor this to use the same PrivKey
            ProvenanceService ssc = new ProvenanceService(PRIVATE_KEY_FILE_NAME);
            QuestionnaireResponseService qrs = new QuestionnaireResponseService();
            FHIRAtWill faw = new FHIRAtWill(client, ssc);

            KeyPair keyPair = oauthClientUtil.createKeyPair(oauthClientUtil.getPublicKey(cert), pk);

            // ***
            // = 4. Ask the authentication server to sign our certificate =
            //***

            CertificationRequest csr = oauthClientUtil.makeSigningRequest(keyPair, cert);

            String signedCertB64 = oauthClientUtil.requestSignedCert(csr, token, signingUrl);
            CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
            InputStream in = new ByteArrayInputStream(Base64.getDecoder().decode(signedCertB64));
            X509Certificate signedCert = (X509Certificate)certFactory.generateCertificate(in);



            StatusOutput.progressConsole(4, 1, "Signed Cert (our identity certificate):\n" + signedCert.toString());

            //
            // Now we have the signed certificate send it to the FHIR server
            //
            DocumentReference certificateToSendInDocumentReference = ssc.buildCertificateDocumentReference(signedCert, createdPatientResource);

            StatusOutput.progressConsole(4, 2, "Now we have the signed certificate send it to the FHIR server:\n" +
                    jsonParser.encodeResourceToString(certificateToSendInDocumentReference));


            // ***
            // = 5. Send our identity certificate to the FHIR server as a DocumentReference (and follow up with Provenance for that resource) =
            //***
            StatusOutput.progressConsole(5, 1,
                    "Send our identity certificate to the FHIR server as a DocumentReference (and follow up with Provenance for that resource:\n");

            faw.send(certificateToSendInDocumentReference, createdPatientResource, userId);

            // ***
            // = 6. Send our example QuestionnaireResponse resource (and follow up with Provenance for that resource) =
            //***

            QuestionnaireResponse qr = qrs.buildExampleQuestionnaireResponse(createdPatientResource);

            StatusOutput.progressConsole(6, 1,
                    "Send our identity certificate to the FHIR server as a DocumentReference (and follow up with Provenance for that resource):\n" +
                    jsonParser.encodeResourceToString(qr));

            faw.send(qr, createdPatientResource, userId);

            StatusOutput.progressConsole(7, 1,
                    "===\n" +
                            "Example complete\n" +
                            "===");

        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (OperatorCreationException e) {
            e.printStackTrace();
        }

    }

}