/*
 * Copyright (c) 2020. Boston Children's Hospital
 */

package org.chip.ihl.Models;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
/**
 * A POJO Signing Request. csrB64 is a string with a Base64-encoded signing/certification request
 */
public class SigningRequest {
    private String csrB64;
}