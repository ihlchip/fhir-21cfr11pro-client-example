/*
 * Copyright (c) 2020. Boston Children's Hospital
 */

package org.chip.ihl.utility;

import java.io.Serializable;

public class Creds implements Serializable {
    private String username;
    private String password;

    public Creds() {
    }

    public Creds(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
