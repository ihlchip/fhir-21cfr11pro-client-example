/*
 * Copyright (c) 2020. Boston Children's Hospital
 */

package org.chip.ihl.utility;

import com.google.gson.*;

import java.util.*;

public class JSONUtils {

    static private String EXCLUDE_ATTRIBUTE_NAMES[] = { "id", "meta" };

    /**
     * Make a proto-canonical version of a JSON object. The real canonical representation is the .toString() version of
     * the returned JsonElement.
     *
     * Thanks to https://stackoverflow.com/a/4803346/6335853 for the original
     *
     * @param srcJsonElement The JsonElement/JsonObject to "canonicalise"
     * @param root boolean to flag if the mutable attributes should be removed from this iteration
     * @return A JsonElement with ordered attributes
     */
    public JsonElement canonicalize(JsonElement srcJsonElement, boolean root) {
        Set<String> filterKeys = new HashSet<String>();

        for (String key: EXCLUDE_ATTRIBUTE_NAMES) {
            filterKeys.add(key);
        }

        if (srcJsonElement instanceof JsonArray) {
            // Canonicalize each element of the array
            JsonArray srcArray = (JsonArray) srcJsonElement;
            JsonArray result = new JsonArray();
            for (int i = 0; i < srcArray.size(); i++) {
                result.add(canonicalize(srcArray.get(i), false));
            }
            return result;
        } else if (srcJsonElement instanceof JsonObject) {
            // Sort the attributes by name, and the canonicalize each element of the object
            JsonObject srcObject = (JsonObject)srcJsonElement;
            JsonObject result = new JsonObject();
            // TreeSet is ordered...
            TreeSet<String> attributes = new TreeSet<>();
            for (Map.Entry<String, JsonElement> entry : srcObject.entrySet()) {
                String thisKey = entry.getKey();
                if (root && filterKeys.contains(thisKey))
                {
                    // skip
                } else{
                    attributes.add(thisKey);
                }
            }
            for (String attribute : attributes) {
                result.add(attribute, canonicalize(srcObject.get(attribute),false));
            }
            return result;
        } else {
            return srcJsonElement;
        }
    }

    /**
     *
     * Convenience wrapper for the recursive canonicalize method
     *
     * @param jsonStringSource
     * @return
     */
    public JsonElement toCanonicalisableFhirJsonElement(String jsonStringSource) {
        JsonObject jsonObject = JsonParser.parseString(jsonStringSource).getAsJsonObject();
        JsonElement jsElem = canonicalize(jsonObject, true);

        return jsElem;
    }
}
