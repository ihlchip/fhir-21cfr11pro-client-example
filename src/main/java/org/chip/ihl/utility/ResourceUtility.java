/*
 * Copyright (c) 2020. Boston Children's Hospital
 */

package org.chip.ihl.utility;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

/**
 *
 * Utility class for accessing resource file content
 *
 * NB: Not FHIR resources, Java/system resources.
 *
 */
public class ResourceUtility {

    /**
     * Load an entire resource into a String
     * @param fileName
     * @param currentClass
     * @return
     */
    public String getResourceFileAsString(String fileName, Class currentClass) {
        InputStream is = getResourceFileAsInputStream(fileName, currentClass);
        if (is != null) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            return reader.lines().collect(Collectors.joining(System.lineSeparator()));
        } else {
            return null;
        }
    }

    /**
     * 
     * Set up an input stream for a resource file
     *
     * @param fileName
     * @param currentClass
     * @return
     */
    public InputStream getResourceFileAsInputStream(String fileName, Class currentClass) {
        ClassLoader classLoader = currentClass.getClassLoader();
        return classLoader.getResourceAsStream(fileName);
    }



}
