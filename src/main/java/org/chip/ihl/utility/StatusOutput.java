/*
 * Copyright (c) 2020. Boston Children's Hospital
 */

package org.chip.ihl.utility;

import java.io.BufferedReader;
import java.io.StringReader;

public class StatusOutput {
    static private Integer cachedStep = 0;
    static private Integer cachedSubStep= 0;

    public static void progressConsole(Integer step, Integer subStep, String writeMeOut) {
        Integer thisStep = (step == 0) ? cachedStep : (cachedStep = step);
        Integer thisSubStep = (subStep == 0) ? cachedSubStep = cachedSubStep + 1 : (cachedSubStep = subStep);


        String stepPrefix = String.format(">Step %01d.%01d | ", thisStep, thisSubStep);
        BufferedReader bufReader = new BufferedReader(new StringReader(writeMeOut));
        String line = null;
        try {
            while( (line = bufReader.readLine()) != null ) {
                System.out.println(stepPrefix + line);
            }

        } catch (Exception ex) {
            System.out.println("Error: Bad string");
        }
    }

}
