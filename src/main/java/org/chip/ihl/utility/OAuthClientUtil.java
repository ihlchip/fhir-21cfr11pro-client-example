/*
 * Copyright (c) 2020. Boston Children's Hospital
 */

package org.chip.ihl.utility;

import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.client.interceptor.BearerTokenAuthInterceptor;
import lombok.extern.java.Log;
import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.asn1.pkcs.CertificationRequest;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.asn1.x509.Certificate;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.bouncycastle.pkcs.jcajce.JcaPKCS10CertificationRequestBuilder;
import org.springframework.http.*;
import org.springframework.web.client.RestOperations;
import org.springframework.web.client.RestTemplate;
import org.json.*;
import org.chip.ihl.Models.*;

import java.io.*;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * OAuth operations
 */
@Log
public class OAuthClientUtil {

    private ResourceUtility ru;
    private Class thisClass;

    public OAuthClientUtil() {
        ru = new ResourceUtility();
        thisClass = OAuthClientUtil.class;
    }

    /**
     *
     * Make the FHIR client work with OAuth bearer authentication
     *
     * @param client - the HAPI FHIR REST client object
     * @param token - the OAuth bearer token we got in exchange for our username/password credentials
     */
    public void registerauthInterceptor(IGenericClient client, String token) {
        BearerTokenAuthInterceptor authInterceptor = new BearerTokenAuthInterceptor(token);

        // Create a client and post the transaction to the server
        client.registerInterceptor(authInterceptor);

    }

    /**
     *
     * getToken Is the OAuth2 exchange of the long-lived credentials (username and password) for a short-lived credential
     * (a bearer token).
     *
     * @param userId
     * @param password
     * @param authUrl
     * @return a bearer token
     */

    public String getToken(String userId, String password, String authUrl) {

        RestTemplate restTemplate = new RestTemplate();


        // Send the client id and secret key in the header to keep confidential in the HTTPS connection.
        // Sending in the query string would expose them.
        String credentials = userId + ":" + password;
        String encodedCredentials = new String(Base64.encodeBase64(credentials.getBytes()));

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Authorization", "Basic " + encodedCredentials);
        headers.add("Content-type", "application/json");

        Creds creds = new Creds(userId, password);
        HttpEntity<Creds> request = new HttpEntity<>(creds, headers);
        Map<String, String> uriVariables = new HashMap();
        uriVariables.put("grant_type", "authorization_code");

        ResponseEntity<String> response = restTemplate.postForEntity(authUrl, request, String.class, uriVariables);

        log.fine("Access Token Response: " + response.getBody());
        JSONObject obj = new JSONObject(response.getBody());
        String accessToken = obj.getString("access_token");
        log.fine("Access Token: " + accessToken);
        return accessToken;
    }



    /**
     *
     * getToken Is the OAuth2 exchange of the long-lived credentials (username and password) for a short-lived credential
     * (a bearer token).
     *
     * @param csr
     * @param bearerToken
     * @param signingUrl
     * @return a bearer token
     */

    public String requestSignedCert(CertificationRequest csr, String bearerToken, String signingUrl) throws IOException {

        RestTemplate restTemplate = new RestTemplate();

        SigningRequest signingRequest = new SigningRequest();
        signingRequest.setCsrB64(new String(Base64.encodeBase64(csr.getEncoded())));

        // Send the bearer token in the header to keep confidential in the HTTPS connection.
        // Sending in the query string would expose them.

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Authorization", "Bearer " + bearerToken);
        headers.add("Content-type", "application/json");

        HttpEntity<SigningRequest> request = new HttpEntity<>(signingRequest, headers);

        ResponseEntity<String> response = restTemplate.postForEntity(signingUrl, request, String.class);

        String signedCertB64 = response.getBody();
        log.fine("signedCertB64:\n" + signedCertB64);
        return signedCertB64;
    }

    /**
     *
     * Retrieve a certificate from a PEM file
     *
     * @param filename
     * @return
     * @throws CertificateException
     * @throws FileNotFoundException
     */
    public X509Certificate pemToCert(String filename) throws CertificateException, FileNotFoundException {
        CertificateFactory fact = CertificateFactory.getInstance("X.509");
        InputStream is = ru.getResourceFileAsInputStream(filename, thisClass);
        X509Certificate cert = (X509Certificate) fact.generateCertificate(is);
        return cert;
    }


    /**
     *
     * retrieve a private key from a file
     *
     * @param filename
     * @return
     * @throws IOException
     */
    public PrivateKey getPrivateKey(String filename) throws IOException {

        PrivateKey key;

        Reader reader = new InputStreamReader(ru.getResourceFileAsInputStream(filename, thisClass));

        try (PEMParser pem = new PEMParser(reader)) {
            JcaPEMKeyConverter jcaPEMKeyConverter = new JcaPEMKeyConverter();
            Object pemContent = pem.readObject();
            if (pemContent instanceof PEMKeyPair) {
                PEMKeyPair pemKeyPair = (PEMKeyPair) pemContent;
                KeyPair keyPair = jcaPEMKeyConverter.getKeyPair(pemKeyPair);
                key = keyPair.getPrivate();
            } else if (pemContent instanceof PrivateKeyInfo) {
                PrivateKeyInfo privateKeyInfo = (PrivateKeyInfo) pemContent;
                key = jcaPEMKeyConverter.getPrivateKey(privateKeyInfo);
            } else {
                throw new IllegalArgumentException("Unsupported private key format '" +
                        pemContent.getClass().getSimpleName() + '"');
            }
        }

        return key;
    }

    /**
     *
     * extract the public key from a certificate
     *
     * @param cert
     * @return
     */

    public PublicKey getPublicKey(X509Certificate cert)
    {
        PublicKey key = cert.getPublicKey();
        return key;
    }

    /**
     *
     * Make a key pair object from a public key and a private key
     *
     * @param pubKey
     * @param privKey
     * @return
     */
    public KeyPair createKeyPair(PublicKey pubKey, PrivateKey privKey) {
        KeyPair kp = new KeyPair(pubKey, privKey);
        return kp;

    }

    /**
     *
     * build a certification request (signing request/CSR) frmo a keypair and a certificate
     *
     * @param kp
     * @param certWithSubject
     * @return
     * @throws OperatorCreationException
     */
    public CertificationRequest makeSigningRequest(KeyPair kp, X509Certificate certWithSubject) throws OperatorCreationException {
        JcaPKCS10CertificationRequestBuilder p10Builder = new JcaPKCS10CertificationRequestBuilder(
                certWithSubject.getSubjectX500Principal(), kp.getPublic());
        JcaContentSignerBuilder csBuilder = new JcaContentSignerBuilder("SHA256withRSA");
        ContentSigner signer = csBuilder.build(kp.getPrivate());
        PKCS10CertificationRequest csr = p10Builder.build(signer);
        return csr.toASN1Structure();
    }
}
