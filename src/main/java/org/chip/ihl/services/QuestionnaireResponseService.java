/*
 * Copyright (c) 2020. Boston Children's Hospital
 */

package org.chip.ihl.services;

import org.hl7.fhir.dstu3.model.*;

/**
 *  Build an example QuestionnaireResponse
 */
public class QuestionnaireResponseService {

    /**
     *
     * Simple method to generate a once-only QuestionnaireResponse
     *
     * @param patient
     * @return
     */
    public QuestionnaireResponse buildExampleQuestionnaireResponse(Patient patient) {

        QuestionnaireResponse qr = new QuestionnaireResponse();
        qr.addItem().setLinkId("Q1").addAnswer().setValue(new StringType("Answer to Q1"));
        qr.setSubject(new Reference(patient));
        qr.setStatus(QuestionnaireResponse.QuestionnaireResponseStatus.COMPLETED);

        return qr;
    }
}
