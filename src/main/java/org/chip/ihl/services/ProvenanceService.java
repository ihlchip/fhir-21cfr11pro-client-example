/*
 * Copyright (c) 2020. Boston Children's Hospital
 */

package org.chip.ihl.services;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import org.bouncycastle.operator.OperatorCreationException;
import org.chip.ihl.utility.JSONUtils;
import org.chip.ihl.utility.OAuthClientUtil;
import org.hl7.fhir.dstu3.model.*;
import org.hl7.fhir.dstu3.model.Signature;

import java.io.IOException;
import java.security.*;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

/**
 * Build provenance for resources that contains the who, what, when of a resource provenance assertion and a
 * digital signature for the resource.
 */
public class ProvenanceService {

    FhirContext ctx = FhirContext.forDstu3();

    OAuthClientUtil clientUtil = new OAuthClientUtil();

    JSONUtils jUtils = new JSONUtils();
    SigningUtilityService signingUtil; //  =

    /**
     * Constructor
     *
     * @param privateKeyFileName the private key filename
     */
    public ProvenanceService(String privateKeyFileName) {

        // Get the private key
        try {
            PrivateKey pk = clientUtil.getPrivateKey(privateKeyFileName);
            this.signingUtil = new SigningUtilityService(pk);
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * Build the FHIR DocumentReference resource that will contain the user's identity certificate
     *  @param cert
     * @param patient
     * @return
     */
    public DocumentReference buildCertificateDocumentReference(X509Certificate cert, Patient patient) {
        Attachment attachment = null;
        try {
            attachment = new Attachment()
                    .setData(cert.getTBSCertificate());
        } catch (CertificateEncodingException e) {
            e.printStackTrace();
        }
        DocumentReference.DocumentReferenceContentComponent docComponent = new DocumentReference.DocumentReferenceContentComponent()
                .setAttachment(attachment);

        List<DocumentReference.DocumentReferenceContentComponent> attachList = new ArrayList<DocumentReference.DocumentReferenceContentComponent>();
        attachList.add(docComponent);
        DocumentReference documentReference = new DocumentReference()
                .setSubject(new Reference(patient))
                .setContent(attachList)
                .setDescription("Certificate for " + patient.getId());

        return documentReference;
    }

    /**
     * Create a Provenance resource for the provided resource. In this example the Patient resource (that identified the
     * client in the FHIR resources) is passed to allow the "who" of Provenance to be built.
     *
     * NB: The OAuth2 principal is also passed, and this should be treated carefully downstream as this information
     * can be identifying whereas a resource like Patient may not be.
     *
     * @param provenanceFor
     * @param patient
     * @param principal
     * @return
     */
    public Provenance createProvenance(Resource provenanceFor, Patient patient, String principal) {
        Signature sig = generateSignature(provenanceFor, patient, principal);
        List<Signature> sigList = new ArrayList<Signature>();

        sigList.add(sig);

        Provenance provenance = new Provenance()
                .addTarget(new Reference(provenanceFor))
                .setSignature(sigList)
                ;
        return provenance;
    }

    /**
     *
     * Generate an (external) signature for the provided resource using the client's private key and the canonical
     * JSON representation of that resource.
     *
     * @param resourceToSign
     * @param patient
     * @param principal
     * @return
     */

    public Signature generateSignature(Resource resourceToSign, Patient patient, String principal) {
        IParser parser = ctx.newJsonParser();
        String serializedResource = parser.encodeResourceToString(resourceToSign);

        Coding sigCoding = new Coding()
                .setSystem("urn:iso-astm:E1762-95:2013")
                .setCode("1.2.840.10065.1.12.1.5")
                .setDisplay("SHA256withRSA external signature of the canonical JSON of the referenced resource");
        ArrayList<Coding> codingList = new ArrayList<Coding>();
        codingList.add(sigCoding);


        Signature generatedSignature = null;
        try {
            generatedSignature = new Signature()
                    .setWho(new Reference(patient))
                    .setWhen(DateTimeType.now().getValue())
                    .setType(codingList)
                    .setBlobElement(new Base64BinaryType(signingUtil.signString(serializedResource)));

        } catch (OperatorCreationException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (SignatureException e) {
            e.printStackTrace();
        }

        return generatedSignature;
    }

}
