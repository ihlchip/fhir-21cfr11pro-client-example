/*
 * Copyright (c) 2020. Boston Children's Hospital
 */

package org.chip.ihl.services;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.api.PreferReturnEnum;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import lombok.extern.java.Log;
import org.chip.ihl.utility.JSONUtils;
import org.chip.ihl.utility.StatusOutput;
import org.hl7.fhir.dstu3.model.Patient;
import org.hl7.fhir.dstu3.model.Provenance;
import org.hl7.fhir.dstu3.model.Resource;
import org.hl7.fhir.instance.model.api.IIdType;

/**
 *
 * FHIRAtWill is a utility class that contains the sequence of events required to
 * send a Provenance-backed resource to a FHIR server.
 *
 */

@Log
public class FHIRAtWill {

    IGenericClient fhirClient;
    FhirContext ctx;
    JSONUtils ju;
    ProvenanceService ssc;
    IParser jsonParser;

    /**
     *
     * Constructor - requires a HAPI FHIR REST client and a Provenance service for
     *
     * @param client
     * @param ssc
     */
    public FHIRAtWill(IGenericClient client, ProvenanceService ssc) {
        this.fhirClient = client;
        this.ju = new JSONUtils();
        this.ctx = this.fhirClient.getFhirContext();
        this.jsonParser = this.ctx.newJsonParser().setPrettyPrint(true);
        this.ssc = ssc;
    }

    /**
     * Send a resource to the HAPI FHIR server verify it is properly represented by the server, then
     * build a Provenance resource for the original resource and persist that on the HAPI FHIR server
     *
     * @param resourceToSend
     * @param me
     * @param principal
     * @return whether the resource and provenance was successfully sent
     */
    public boolean send(Resource resourceToSend, Patient me, String principal) {
        // POST the resource
        StatusOutput.progressConsole(0, 0,
                "Resource to send:\n" + jsonParser.encodeResourceToString(resourceToSend)) ;

        // log.info("Resource to send:\n" + jsonParser.encodeResourceToString(resourceToSend));
        MethodOutcome outcome = fhirClient.create()
                .resource(resourceToSend)
                .prettyPrint()
                .encodedJson()
                .prefer(PreferReturnEnum.REPRESENTATION)
                .execute();

        // Once sent, check that the response copy is substantially the same

        String originalResourceAsJson = jsonParser.encodeResourceToString(resourceToSend);
        String originalFilteredCanonical = ju.toCanonicalisableFhirJsonElement(originalResourceAsJson).toString();
        // Print the output
        IIdType id = outcome.getId();
        Resource createdPatientResource = (Resource) outcome.getResource();

        String returnedResourceAsJson = jsonParser.encodeResourceToString(createdPatientResource);
        String returnedFilteredCanonical = ju.toCanonicalisableFhirJsonElement(returnedResourceAsJson).toString();

        if (!originalFilteredCanonical.equals(returnedFilteredCanonical)) {
            // The server messed up the content. Don't sign or provide provenance.
            // It would be a good idea to tell someone about it, but that's an exercise left for the app
            // developer.
            log.info("Resource from server is NOT the same as resource sent.\n" +
                    "AT THIS POINT THE CLIENT SHOULD RING ALARM BELLS.");
            return false;
        }
        StatusOutput.progressConsole(0, 0,
                "Resource from server is the same as resource sent - generating signed Provenance...\n") ;

        //log.info("Resource from server is the same as resource sent - generating signed Provenance for that resource.");

        // Now generate a provenance record for that resource and POST that
        Provenance provenance = ssc.createProvenance(createdPatientResource, me, principal);

        MethodOutcome provOutcome = fhirClient.create()
                .resource(provenance)
                .prettyPrint()
                .encodedJson()
                .prefer(PreferReturnEnum.REPRESENTATION)
                .execute();
        StatusOutput.progressConsole(0, 0,
                "Provenance stored:\n" + jsonParser.encodeResourceToString(provOutcome.getResource())) ;

        // log.info("Provenance stored:\n" + jsonParser.encodeResourceToString(provOutcome.getResource()));

        return true;
    }
}
