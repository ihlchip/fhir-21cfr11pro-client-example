/*
 * Copyright (c) 2020. Boston Children's Hospital
 */

package org.chip.ihl.services;

import org.bouncycastle.operator.OperatorCreationException;

import java.security.*;

/**
 * A utility class to generate signatures for strings from a string and a private key
 */
public class SigningUtilityService {

    private PrivateKey pk ;
    Signature signature;

    /**
     *
     * Constructor that accepts and stores a private key to use for signing.
     *
     * @param pk
     * @throws InvalidKeyException
     */
    public SigningUtilityService(PrivateKey pk) throws InvalidKeyException {
        this.pk = pk;
        try {
            this.signature = Signature.getInstance("SHA256WithRSA", "BC");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        }

        this.signature.initSign(this.pk);
    }

    /**
     *
     * Sign a string using the private key.
     *
     * @param thingToSign
     * @return an external (detached) signature
     *
     * @throws OperatorCreationException
     * @throws NoSuchProviderException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     * @throws SignatureException
     */
    public byte[] signString(String thingToSign) throws OperatorCreationException, NoSuchProviderException, NoSuchAlgorithmException, InvalidKeyException, SignatureException {

        signature.update(thingToSign.getBytes());

        return signature.sign();
    };

}
