package org.chip.ihl.services;

import ca.uhn.fhir.context.FhirContext;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.hl7.fhir.dstu3.model.Patient;
import org.hl7.fhir.dstu3.model.Provenance;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.security.Security;

class ProvenanceServiceTest {
    ProvenanceService ssc;
    FhirContext ctx = FhirContext.forDstu3();


    @BeforeEach
    void setUp() {
        Security.addProvider(new BouncyCastleProvider());
        ssc = new ProvenanceService("test-certs/key.pem");
    }

    @Test
    void createProvenance() {
        Patient patient = new Patient();
        patient.addIdentifier()
                .setSystem("CN") // NC = invented X.509 Canonical Name system. This could be PT instead (patient external identifier)
                .setValue("test"); // Our patient's principal identifier which is the CN= in our certificate
        patient.addName().setFamily("Patel").addGiven("Sangeetha");

        Provenance patientProvenance = ssc.createProvenance(patient, patient, "test");

        System.out.println("Provenance is: \n" + ctx.newJsonParser().encodeResourceToString(patientProvenance));

    }

}