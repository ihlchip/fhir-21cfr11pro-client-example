package org.chip.ihl.utility;

import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;

class JSONUtilsTest {

    JSONUtils jsonUtils = new JSONUtils();

    String testJsonString = "{\n" +
            "  \"resourceType\": \"Patient\",\n" +
            "  \"id\": \"98\",\n" +
            "  \"meta\": {\n" +
            "    \"versionId\": \"3\",\n" +
            "    \"lastUpdated\": \"2019-12-16T01:16:19.092+00:00\",\n" +
            "    \"source\": \"#LugTK0ygpa6VbC4C\"\n" +
            "  },\n" +
            "  \"text\": {\n" +
            "    \"status\": \"generated\",\n" +
            "    \"div\": \"<div xmlns=\\\"http://www.w3.org/1999/xhtml\\\"> <div class=\\\"hapiHeaderText\\\">Caleb <b>CUSHING </b> </div> <table class=\\\"hapiPropertyTable\\\"> <tbody> <tr> <td>Identifier</td> <td>b15b4794-7fa7-4738-b5d7-0acda0e6705e</td> </tr> </tbody> </table> </div>\"\n" +
            "  },\n" +
            "  \"identifier\": [ {\n" +
            "    \"type\": {\n" +
            "      \"coding\": [ {\n" +
            "        \"system\": \"http://hl7.org/fhir/v2/0203\",\n" +
            "        \"code\": \"MR\"\n" +
            "      } ]\n" +
            "    },\n" +
            "    \"value\": \"b15b4794-7fa7-4738-b5d7-0acda0e6705e\"\n" +
            "  } ],\n" +
            "  \"name\": [ {\n" +
            "    \"family\": \"NewName\",\n" +
            "    \"given\": [ \"NewName\" ]\n" +
            "  } ]\n" +
            "}";

    @BeforeEach
    void setUp() {
    }

    @Test
    void canonicalize() {
        JsonObject jsonObject = JsonParser.parseString(testJsonString).getAsJsonObject();
        JsonElement jsElement = jsonUtils.canonicalize(jsonObject, false);
        String canonicalisedVersion = jsElement.toString();
        System.out.println("Before: \n" + testJsonString + "\n\nAfter: \n" + canonicalisedVersion);
        assertNotEquals(testJsonString, canonicalisedVersion);
    }

    @Test
    void toCanonicalisableFhirJsonElement() {

        // First let's get a canonicalised yet unfiltered version of the JSON
        JsonObject jsonObject = JsonParser.parseString(testJsonString).getAsJsonObject();
        JsonElement jsElement = jsonUtils.canonicalize(jsonObject, false);
        String canonicalisedVersion = jsElement.toString();

        // The original has these keys
        assertTrue(canonicalisedVersion.contains("\"id\":"));
        assertTrue(canonicalisedVersion.contains("\"meta\":"));

        // Now let's generate the filtered version
        JsonElement fhirCanonical = jsonUtils.toCanonicalisableFhirJsonElement(testJsonString);
        String fhirCanonicalString = fhirCanonical.toString();

        // They should not be the same (filter shoudl remove the id and meta branches)
        assertNotEquals(canonicalisedVersion, fhirCanonicalString);

        // the filtered canonical version does not have these keys
        assertFalse(fhirCanonicalString.contains("\"id\":"));
        assertFalse(fhirCanonicalString.contains("\"meta\":"));
        System.out.println("fhirCanonicalString:\n"+ fhirCanonicalString);

    }
}